import {Add, Sub, Operation, calculator} from 'calculator'

describe('Operace', () => {

  describe('sčítání (TODO 2)', () => {
    it('má jméno add', () => {
      const add = new Add();
      expect(add.name).toBe('add');
    });

    // TODO 2.1 - vytvořte test "počítá součet"
    it("počítá součet", () => {
      const add = new Add();
      expect(add.calculate(1, 2)).toBe(3);
    });

    // TODO 2.2 - vytvořte test "ošetřuje vstupy"
    it("ošetřuje vstupy", () => {
      const add = new Add();
      expect(add.calculate("1", 2)).toBe(3);
    });
  });

  describe('odčítání (TODO 3.2)', () => {
    it('má jméno sub', () => {
      const sub = new Sub();
      expect(sub.name).toBe('sub');
    });

    it('počítá rozdíl', () => {
      const sub = new Sub();
      expect(sub.calculate(2, 1)).toBe(1);
    });

    it('ošetřuje vstupy', () => {
      const sub = new Sub();
      expect(sub.calculate("2a", "1")).toBe(1);
    });
  });

  describe('využívají dědičnost (TODO 3.4)', () => {

    it('sčítání je potomkem Operation', () => {
      const add = new Add();
      expect(add instanceof Operation).toBeTruthy();
      expect(add instanceof Object).toBeTruthy();

      expect(Operation.prototype.isPrototypeOf(add)).toBeTruthy();
      expect(Object.prototype.isPrototypeOf(add)).toBeTruthy();
    });

    it('odčítání je potomkem Operation', () => {
      const sub = new Sub();
      expect(sub instanceof Operation).toBeTruthy();
      expect(sub instanceof Object).toBeTruthy();

      expect(Operation.prototype.isPrototypeOf(sub)).toBeTruthy();
      expect(Object.prototype.isPrototypeOf(sub)).toBeTruthy();
    });

  });
});

describe('Kalkulačka', () => {
  it('má akci pro sčítání', () => {
    expect(calculator.calculate('add', 1, 2)).toBe(3);
  });

  it('má akci pro odčítání (TODO 3.3)', () => {
    expect(calculator.calculate('sub', 2, 1)).toBe(1);
  });
});
