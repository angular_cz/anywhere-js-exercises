import React from 'React';

import Display from './display'
import Buttons from './buttons'

import {Add, Sub, Mul, Div} from '../calculations/operations';

export default class Calculator extends React.Component {
  constructor() {
    super();
    this.state = Calculator.initialState;

    this.operations = {
      'plus': new Add(),
      'minus': new Sub(),
      'mul': new Mul(),
      'div': new Div()
    }
  }

  setOperand(operand) {
    this.setState({
      operand,
      // TODO 2.1 zapište operand do state displayValue, aby se zobrazoval na displeji
      displayValue: operand
    });
  }

  processOperation(type) {
    switch (type) {
      case "clear":
        // TODO 2.2 obnovte stav do initialState
        this.setState(Calculator.initialState);
        break;

      case "equals":
        var result = this.calculateResult_();
        this.setState({
          result,
          displayValue: result
        });
        break;

      default:
        if (this.state.result === null || !this.state.operation) {  // first operation
          this.setState({
            operation: type,
            result: this.state.operand
          });
        } else {
          this.setState({
            operation: type,
            operand: this.state.result
          });
        }
    }
  }

  render() {  // TODO 1.3 komponentě display předávejte hodnotu ze stavu
    return <div className="calculator">
      <Display value={this.state.displayValue}/>
      <Buttons
          onOperandChange={this.setOperand.bind(this)}
          onOperation={this.processOperation.bind(this)}/>
    </div>
  }

  calculate_(name, a, b) {
    if (!this.operations[name]) {
      throw new TypeError('Unknown operation: ' + name);
    }

    return this.operations[name].calculate(a, b);
  }

  calculateResult_() {
    if (this.state.result === null) {
      return 0;
    }

    if (this.state.operation) {
      return this.calculate_(this.state.operation, this.state.result, this.state.operand);
    }

    return this.state.result;
  }
}

Calculator.initialState = {
  operand: null,
  result: null,
  operation: null,
  displayValue: 0
};

