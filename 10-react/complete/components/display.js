import React from 'React';

let Display = (props) => {      // TODO 1.1 -  definujte display - input pouze pro čtení
  return <div className="display">
    <input type="number"
           value={props.value}
           readOnly/>
  </div>

};

// TODO 1.2 -  přiřaďte definici typů propTypes jako vlastnost funkce Display

Display.propTypes = {
  value: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ]).isRequired
};

export default Display