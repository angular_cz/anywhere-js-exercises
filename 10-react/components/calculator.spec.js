import React from 'React'
import Calculator from 'calculator'

describe('Calculator', () => {

  const render = (element) => {
    let renderer = React.addons.TestUtils.createRenderer();
    renderer.render(element);
    return renderer.getRenderOutput();
  };

  it('je definovaný', () => {
    expect(Calculator).toBeDefined();
  });

  it('zapisuje stisknutá tlačítka na display (TODO 2.1)', () => {
    let dom = React.addons.TestUtils.renderIntoDocument(<Calculator />);
    let buttons = React.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');
    let firstButton = buttons[0];
    let input = React.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

    React.addons.TestUtils.Simulate.click(firstButton);

    expect(input.value).toEqual(firstButton.value);

    React.addons.TestUtils.Simulate.click(firstButton);

    expect(input.value).toEqual(firstButton.value + firstButton.value);

  });


  it('počítá součet', () => {
    let filterByName = (name) => (button) => button.name === name;

    let dom = React.addons.TestUtils.renderIntoDocument(<Calculator />);
    let input = React.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

    let buttons = React.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');
    let firstButton = buttons[0];

    let operations = React.addons.TestUtils.scryRenderedDOMComponentsWithTag(dom, 'button');
    let plusButton = operations.filter(filterByName('plus'))[0];
    let equalsButton = operations.filter(filterByName('equals'))[0];

    React.addons.TestUtils.Simulate.click(firstButton);
    React.addons.TestUtils.Simulate.click(plusButton);
    React.addons.TestUtils.Simulate.click(firstButton);
    React.addons.TestUtils.Simulate.click(equalsButton);

    expect(input.value).toEqual(String(parseInt(firstButton.value) + parseInt(firstButton.value)));

  });

  it('aplikuje operaci znovu při dalším stisku rovná se', () => {
    let filterByName = (name) => (button) => button.name === name;

    let dom = React.addons.TestUtils.renderIntoDocument(<Calculator />);
    let input = React.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

    let buttons = React.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');
    let firstButton = buttons[0];

    let operations = React.addons.TestUtils.scryRenderedDOMComponentsWithTag(dom, 'button');
    let plusButton = operations.filter(filterByName('plus'))[0];
    let equalsButton = operations.filter(filterByName('equals'))[0];

    React.addons.TestUtils.Simulate.click(firstButton);
    React.addons.TestUtils.Simulate.click(plusButton);
    React.addons.TestUtils.Simulate.click(firstButton);
    React.addons.TestUtils.Simulate.click(equalsButton);

    expect(input.value).toEqual(String(2 * parseInt(firstButton.value)));

    React.addons.TestUtils.Simulate.click(equalsButton);
    expect(input.value).toEqual(String(3 * parseInt(firstButton.value)));
  });


  it('nuluje display po kliknutí na clear (TODO 2.2)', () => {
    let filterByName = (name) => (button) => button.name === name;

    let dom = React.addons.TestUtils.renderIntoDocument(<Calculator />);
    let buttons = React.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');
    let firstButton = buttons[0];
    let operations = React.addons.TestUtils.scryRenderedDOMComponentsWithTag(dom, 'button');
    let clearButton = operations.filter(filterByName('clear'))[0];
    let input = React.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

    React.addons.TestUtils.Simulate.click(firstButton);

    expect(input.value).not.toEqual('0');

    React.addons.TestUtils.Simulate.click(clearButton);

    expect(input.value).toEqual('0');

  });

});
