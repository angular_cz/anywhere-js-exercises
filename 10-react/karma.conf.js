var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.files.push('bower_components/react/react-with-addons.js');
  config.files.push('bower_components/react/react-dom.js');

  config.files.push('10-react/transpiled/calculations/operations.js');

  config.files.push('10-react/transpiled/components/button.js');
  config.files.push('10-react/transpiled/components/operation.js');
  config.files.push('10-react/transpiled/components/display.js');

  config.files.push('10-react/transpiled/components/buttons.js');
  config.files.push('10-react/transpiled/components/calculator.js');

  config.files.push('10-react/transpiled/components/display.spec.js');
  config.files.push('10-react/transpiled/components/calculator.spec.js');



};
