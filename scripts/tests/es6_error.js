(function(global) {

  global.__processError = function(error) {

    switch (error.name) {
      case "SyntaxError":
        throw new SyntaxError('during transpilation in ' + error.message);

      default:
        throw new Error('during transpilation, type: ' + error.name + ' in ' + error.message);

    }

  };

}(window));
