const generatorFactory = function(initialNumber) {
  initialNumber = initialNumber || 0;

  return function generator() {
    return initialNumber++;
  }
};

window.baseGeneratorFactory = generatorFactory;
