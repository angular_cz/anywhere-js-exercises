const generatorFactory = function (initialNumber) {

  let step = 5;
  let value = initialNumber || 0;

  function setStep(newStep) {
    step = newStep;
  }

  function increase() {
    value += step;
    return value;
  }

  return {
    setStep: setStep,
    increase: increase
  };

};

window.stepGeneratorFactory = generatorFactory;

