import {Calculator, Operation, UnknownOperationError} from './calculator'

describe('Kalkulačka', () => {

  it('může být instancována', () => {
    expect((new Calculator()) instanceof Calculator).toBeTruthy();
  });

  it('může mít přidánu operaci pod jménem', () => {
    const calc = new Calculator();
    const operation = new Operation();
    operation.name = 'op';

    calc.addOperation(operation);

    expect(calc.operations['op']).toBe(operation);
  });

  it('umožuje provolat operaci', () => {
    const calc = new Calculator();
    const operation = new Operation();
    operation.name = 'op';

    spyOn(operation, "calculate").and.returnValue(42);

    calc.addOperation(operation);

    expect(calc.calculate('op', 40, 2)).toBe(42);
    expect(operation.calculate).toHaveBeenCalledWith(40, 2);
  });

  describe('vyhodí výjimku', () => {
    it('TypeError, při pokusu o přidání akce, která není potomkem Operation (TODO 1)', () => {

      const calculator = new Calculator();

      expect(
        function () {
          calculator.addOperation({});
        }
      ).toThrowError(TypeError);
    });

    it('UnknownOperationError, při volání neznámé operace (TODO 3)', () => {
      const calculator = new Calculator();
      expect(() => {
        calculator.calculate("unknown")
      }).toThrowError(UnknownOperationError);
    });

    it('s názvem operace, při volání neznámé operace (TODO 3)', () => {
      const calculator = new Calculator();

      function throwFunction() {
        calculator.calculate("unknown-op")
      }

      expect(throwFunction).toThrowError(UnknownOperationError);
      expect(throwFunction).toThrowError(/unknown-op/);
    });
  });

});

describe('Výjimka UnknownOperationError (TODO 2)', () => {
  it('může být instancována', () => {
    const error = new UnknownOperationError();

    expect(error instanceof UnknownOperationError).toBeTruthy();
  });

  it('má správný name', () => {
    const error = new UnknownOperationError();

    expect(error.name).toBe('UnknownOperationError');
  });

  it('je potomkem Error', () => {
    const error = new UnknownOperationError();

    expect(error instanceof Error).toBeTruthy();
  });

  it('obsahuje předanou message', () => {
    const throwFunction = () => {
      throw new UnknownOperationError("my message");
    };

    expect(throwFunction).toThrowError(UnknownOperationError, "my message");
  });

  it('má svůj konstruktor', () => {
    const error = new UnknownOperationError();
    expect(error.constructor).toBe(UnknownOperationError);
  });

});
