export class Operation {
  constructor(name) {
    this.name = name;
  }

  calculate(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };
}

export class Add extends Operation {

  constructor() {
    super('add');
  }

  calculateInternal_(a, b) {
    return a + b;
  };
}

export class Sub extends Operation {

  constructor() {
    super('sub');
  }

  calculateInternal_(a, b) {
    return a - b;
  };
}

export class UnknownOperationError extends Error {

  constructor(message) {
    super(message);

    this.name = 'UnknownOperationError';
  }

}

export class Calculator {
  constructor() {
    this.operations = new Map();
  }

  addOperation(operation) {
    if (!(operation instanceof Operation)) {
      throw new TypeError("operation must be instance of Operation");
    }

    this.operations.set(operation.name, operation);
  };

  calculate(name, ...operands) {
    if (!this.operations.has(name)) {
      throw new UnknownOperationError(`Unknown operation: ${name}`);
    }

    return this.operations.get(name).calculate(...operands);
  }
}

export let calculator = new Calculator();
calculator.addOperation(new Add());
calculator.addOperation(new Sub());
