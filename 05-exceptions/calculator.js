
export class Operation {
  constructor(name) {
    this.name = name;
  }

  calculate(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };
}

export class Add extends Operation {

  constructor() {
    super('add');
  }

  calculateInternal_(a, b) {
    return a + b;
  };
}

export class Sub extends Operation {

  constructor() {
    super('sub');
  }

  calculateInternal_(a, b) {
    return a - b;
  };
}

// TODO 2 - Vytvořte výjimku UnknownOperationError

export class Calculator {
  constructor() {
    this.operations = new Map();
  }

  addOperation(operation) {
    // TODO 1.2 - Ošetření přidání neplatné operace

    this.operations.set(operation.name, operation);
  };

  calculate(name, ...operands) {
    // TODO 3 - Ošetření neznámé operace

    return this.operations.get(name).calculate(...operands);
  }
}

export let calculator = new Calculator();
calculator.addOperation(new Add());
calculator.addOperation(new Sub());

