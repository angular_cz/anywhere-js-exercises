import { ButtonControls } from 'buttonControls'

describe('ButtonControls', function() {

  beforeEach(function() {
    this.buttonsElement = jasmine.createSpyObj('buttonsElement', ['addEventListener']);
    this.display = jasmine.createSpyObj('display', ['setValue', 'clear', 'addToValue', 'getValue']);
    this.calculator = jasmine.createSpyObj('calculator', ['setOperand', 'setOperation', 'clear', 'equals']);

    spyOn(ButtonControls.prototype, 'processButton')
      .and.callThrough();

    this.buttons = new ButtonControls(this.buttonsElement, this.calculator, this.display);

    this.buttonEvent = {
      target: {
        type: 'button',
        classList: {
          contains: function() {
          }
        },
        getAttribute: function(param) {
          return this.buttonEvent.target[param];
        }.bind(this)
      }
    };
  });

  describe('registruje událost click', function() {

    beforeEach(function() {

      this.callClickListener = function() {
        let contextOfProcessButton;
        ButtonControls.prototype.processButton.and.callFake(function() {
          contextOfProcessButton = this;
        });

        let clickListener = this.buttonsElement.addEventListener.calls.mostRecent().args[1];
        clickListener();

        return contextOfProcessButton;
      }
    });

    it('na buttonsElement (TODO 2.1)', function() {
      expect(this.buttonsElement.addEventListener).toHaveBeenCalled();

      const eventName = this.buttonsElement.addEventListener.calls.mostRecent().args[0];
      expect(eventName).toBe("click");
    });

    it('s metodou processButton (TODO 2.1)', function() {
      this.callClickListener();

      expect(this.buttons.processButton).toHaveBeenCalled();
    });

    it('a zachovává kontext volání (TODO 2.1)', function() {
      const contextOfProcessButton = this.callClickListener();

      expect(this.buttons).toBe(contextOfProcessButton);
    });
  });

  it('při stisku čísla je aktualizován display (TODO 3.1)', function() {
    this.buttonEvent.target.value = 4;

    this.buttons.processButton(this.buttonEvent);

    expect(this.display.addToValue).toHaveBeenCalledWith(4);
  });

  describe('při stisku operace(TODO 3.2)', function() {
    beforeEach(function() {
      const operationsCheck = function(value) {
        return value === "operation";
      };
      spyOn(this.buttonEvent.target.classList, "contains").and.callFake(operationsCheck);
    });

    it('clear vymaže display i calculator', function() {

      this.buttonEvent.target.name = 'clear';

      this.buttons.processButton(this.buttonEvent);

      expect(this.display.clear).toHaveBeenCalled();
      expect(this.calculator.clear).toHaveBeenCalled();
    });

    it('equals počítá výsledek', function() {

      this.buttonEvent.target.name = 'equals';

      this.buttons.processButton(this.buttonEvent);

      expect(this.calculator.equals).toHaveBeenCalled();
    });

    it('jiné operace se volá setOperation', function() {

      this.buttonEvent.target.name = 'mul';

      this.buttons.processButton(this.buttonEvent);

      expect(this.calculator.setOperation).toHaveBeenCalledWith("mul");
    });

    it('není volána metoda pro změnu čísla', function() {

      this.buttonEvent.target.name = 'plus';

      this.buttons.processButton(this.buttonEvent);

      expect(this.display.addToValue).not.toHaveBeenCalled();
    });
  })

  it('nesmí reagovat na click jiné události (TODO 3.3)', function() {
    this.buttons.processButton({target: {type: 'none'}});

    expect(this.calculator.setOperand).not.toHaveBeenCalled();
    expect(this.calculator.clear).not.toHaveBeenCalled();
    expect(this.display.clear).not.toHaveBeenCalled();
  });
});
