import {Display} from 'display'

describe('Display', function() {
  beforeEach(function() {
    this.element = {
      set value(value) {
        this.setAttribute("value", value);
      },
      setAttribute: function(attr, value) {
      }
    };

    spyOn(this.element, 'setAttribute');
  });

  it('renderuje data do elementu', function() {
    const display = new Display(this.element);

    expect(this.element.setAttribute).toHaveBeenCalledWith("value", "0");
  });
});
