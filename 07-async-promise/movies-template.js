function movieToHtml(movie) {
  return `<p>${movie.rank}. ${movie.name} (${movie.year}) - ${movie.rating}%</p>`;
}

export function moviesToHtml(movies) {

  const moviesHtml = movies.map(movieToHtml).join('');

  return `<div class="column">
  <div class="well">
    <h3> Top ${movies.length}</h3>
    ${moviesHtml}
  </div></div>`;
}
