import { timeoutPromiseFactory } from 'timeout-promise';

describe('timeoutPromiseFactory', function() {

  jasmine.DEFAULT_TIMEOUT_INTERVAL = 150;

  describe('je definována jako funkce', function() {

    it("která vrací promise (TODO 1.1)", function() {

      const timeout = timeoutPromiseFactory(1, function() {});
      expect(timeout instanceof Promise).toBe(true);
    });

  });

  it("hodnota je stejná jako návratová hodnota conditionFunction (TODO 1.2)", function(done) {

    const conditionFunction = function() {
      return 42;
    };

    const timeout = timeoutPromiseFactory(1, conditionFunction);

    timeout.then(function(value) {
      expect(value).toBe(42);
      done();
    });
  });

  it("pokud funkce vrátí null je promise rejected (TODO 1.3)", function(done) {
    const conditionFunction = function() {
      return null;
    };

    const timeout = timeoutPromiseFactory(1, conditionFunction);

    timeout.catch(function(value) {
      expect(value).toBe(null);
      done();
    });
  });

  describe("conditionFunction", function() {

    beforeEach(function() {
      jasmine.clock().install();
    });

    afterEach(function() {
      jasmine.clock().uninstall();
    });

    it("je volána s definovaným zpožděním (TODO 1.4)", function() {

      let wasCalled = false;

      const conditionFunction = function() {
        wasCalled = true;
      };

      const timeout = timeoutPromiseFactory(100, conditionFunction);

      expect(wasCalled).toBe(false);

      jasmine.clock().tick(99);

      expect(wasCalled).toBe(false);

      jasmine.clock().tick(10);

      expect(wasCalled).toBe(true);

    });

  })

});
